/**
 * Created by nhoaiphuong on 2/14/2017.
 */
export default require( 'knex' )( {

    client: 'mysql',
    connection: {
        host: 'localhost',
        user: 'root',
        password: '',
        database: 'birdbase',
        charset: 'utf8'
    }
} );