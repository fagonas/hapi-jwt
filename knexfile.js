/**
 * Created by nhoaiphuong on 2/14/2017.
 */
module.exports = {

    development: {

        migrations: { tableName: 'knex_migrations' },
        seeds: { tableName: './seeds' },

        client: 'mysql',
        connection: {

            host: 'localhost',

            user: 'root',
            password: '',

            database: 'birdbase',
            charset: 'utf8'
        }
    }
};